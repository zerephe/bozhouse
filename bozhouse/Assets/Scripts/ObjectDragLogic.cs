﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Firebase.Database;
using Firebase;
using Firebase.Unity.Editor;
using TMPro;

public class ObjectDragLogic : MonoBehaviour {

    private Firebase.Auth.FirebaseAuth auth;
    private Firebase.Auth.FirebaseUser user;
    private DatabaseReference mReference;

    //Initialize Variables
    public GameObject getTarget;
    public GameObject touchHandler;
    public GameObject baseOBJ;
	public GameObject tayak;
    public TMP_Text scoreField;
    public GameObject congrats;

    bool isMouseDragging, isPlane = false;
    Vector3 offsetValue;
    Vector3 positionOfScreen;

    //object variables
    private bool baseObject = true;

    private Vector3 initialPos;
    private Vector3 basePos;
    private Quaternion initialRot, nullRot;

    private IDictionary<string, int> order = new Dictionary<string, int>();
    private ArrayList checker = new ArrayList();
    private int orderCount = 1;
    private int score = 0;
    bool countFlag = false;

    // Use this for initialization
    void Start() {

        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;
        user = auth.CurrentUser;
        // Set up the Editor before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://bozhouse-001.firebaseio.com/");

        // Get the root reference location of the database.
        mReference = FirebaseDatabase.DefaultInstance.RootReference;

        order.Add("kerege", 1);
		order.Add("tayak", 2);
        order.Add("tunduk", 3);
		order.Add("uuk", 4);
        order.Add("chiy", 5);
        order.Add("lower", 6);
        order.Add("doors", 7);
        order.Add("upper", 8);
        order.Add("upper_uzor", 9);
        order.Add("lower_uzor", 10);
        order.Add("body", 11);

        basePos = baseOBJ.transform.position;
        nullRot.eulerAngles = new Vector3(-90, 0, 0);
    }

    void FixedUpdate() {

        if(orderCount >= order.Keys.Count) {

            congrats.SetActive(true);

            if (user != null) {
                UserScores usr = new UserScores(user.DisplayName, score.ToString());
                string json = JsonUtility.ToJson(usr);
                mReference.Child("bestScores").Child(user.UserId).SetRawJsonValueAsync(json);
            }
            orderCount = 0;
        }

        //Mouse Button Press Down
        if (Input.GetMouseButtonDown(0)) {

            countFlag = true;

            RaycastHit hitInfo;
            getTarget = ReturnClickedObject(out hitInfo);
            if (getTarget != null && !getTarget.name.Equals("Plane") && !checker.Contains(getTarget.name)) {

				touchHandler.GetComponent<PlayerFollow>().enabled = false;

                getTarget.GetComponent<Rigidbody>().isKinematic = true;
                getTarget.GetComponent<Collider>().isTrigger = true;

                initialRot = getTarget.transform.rotation;
                getTarget.transform.rotation = nullRot;

                //checking if object is base object
                if (getTarget.name.Equals("doors_ramka")) {
                    baseObject = true;
                }

                isMouseDragging = true;
                //Converting world position to screen position.
                positionOfScreen = Camera.main.WorldToScreenPoint(getTarget.transform.position);
                offsetValue = getTarget.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, positionOfScreen.z));

                initialPos = getTarget.transform.position;
            }
            else if(getTarget != null) {
                isPlane = true;
            }
        }

        //Mouse Button Up
        if (Input.GetMouseButtonUp(0)) {
            isPlane = false;
            isMouseDragging = false;
			touchHandler.GetComponent<PlayerFollow>().enabled = true;
            getTarget.GetComponent<Collider>().isTrigger = false;


			if (getTarget.name.Equals("doors_ramka") && countFlag) {
                basePos = getTarget.transform.position;
                getTarget.GetComponent<Rigidbody>().isKinematic = true;

                //adding already set in urt object
                checker.Add(getTarget.name);

				score += 100;
                countFlag = false;
            }

            if (!getTarget.name.Equals("Plane") && !baseObject) {
                getTarget.transform.position = initialPos;
                getTarget.transform.rotation = initialRot;
                getTarget.GetComponent<Rigidbody>().isKinematic = false;
            }
            else if(!getTarget.name.Equals("Plane") && countFlag && !checker.Contains(getTarget.name)) {
                float distance = Vector3.Distance(getTarget.transform.position, basePos);
                float maxDistance = 10;

                getTarget.GetComponent<Rigidbody>().isKinematic = false;

                if (distance <= maxDistance && order[getTarget.name.ToString()].Equals(orderCount)) {
                    getTarget.transform.position = basePos;
                    getTarget.GetComponent<Rigidbody>().isKinematic = true;

                    //adding already set in urt object
                    checker.Add(getTarget.name);

                    orderCount++;
                    countFlag = false;
                    
                    score += 100;

                    scoreField.text = score.ToString() + " pts";

					if (getTarget.name.Equals ("uuk")) {
						tayak.SetActive(false);
					}
                    //adding kerege into checkers if other element was dragged before it
                    if (!checker.Contains(baseOBJ.name)) {
                        checker.Add(baseOBJ.name);
                    }
                }
                else if(distance <= 50 && countFlag){
                    getTarget.transform.position = initialPos;
                    score -= 25;
                    countFlag = false;
                    scoreField.text = score.ToString() + " pts";
                }
                Debug.Log(distance);
                Debug.Log(basePos);
            }
        }

        //Is mouse Moving
        if (isMouseDragging && !isPlane) {
            //tracking mouse position.
            Vector3 currentScreenSpace = new Vector3(Input.mousePosition.x, Input.mousePosition.y, positionOfScreen.z);

            //converting screen position to world position with offset changes.
            Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenSpace) + offsetValue;

            //It will update target gameobject's current postion.
            getTarget.transform.position = new Vector3(currentPosition.x, Mathf.Clamp(currentPosition.y, 0, 100), currentPosition.z);
        }

    }

    //Method to Return Clicked Object
    GameObject ReturnClickedObject(out RaycastHit hit) {
        GameObject target = null;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray.origin, ray.direction * 10, out hit)) {
            target = hit.collider.gameObject;
        }

        return target;
    }
}

public class UserScores {

    public string userName;
    public string score;

    public UserScores() { }

    public UserScores(string userName, string score) {
        this.userName = userName;
        this.score = score;
    }
}