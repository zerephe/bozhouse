﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchLogic : MonoBehaviour {

	//
	// VARIABLES
	//
	public Camera cam;
	public Transform transformTarget;
	public GameObject touchHander;

	public float turnSpeed = 4.0f;      // Speed of camera turning when mouse moves in along an axis
	public float panSpeed = 4.0f;       // Speed of the camera when being panned
	public float zoomSpeed = 0.5f;      // Speed of the camera going back and forth

	private Vector3 mouseOrigin;    // Position of cursor when mouse dragging starts
	private bool isPanning;     // Is the camera being panned?
	private bool isRotating;    // Is the camera being rotated?
	private bool isZooming;     // Is the camera zooming?

	//
	// UPDATE
	//

	void FixedUpdate() {

		if (Input.touchCount == 2) {

			//disabling dragging while zooming
			touchHander.GetComponent<ObjectDragLogic>().enabled = false;

			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);

			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

			Debug.Log(touchOnePrevPos);

			float prevTouchDeltaMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMagnitude = (touchZero.position - touchOne.position).magnitude;
			float deltaMagnitudeDiff = prevTouchDeltaMagnitude - touchDeltaMagnitude;

			Debug.Log(deltaMagnitudeDiff);

			if (cam.orthographic) {
				cam.orthographicSize += deltaMagnitudeDiff * zoomSpeed;
				cam.orthographicSize = Mathf.Max(cam.orthographicSize, .1f);
			}
			else {
				cam.fieldOfView += deltaMagnitudeDiff/4 * zoomSpeed;
				cam.fieldOfView = Mathf.Clamp(cam.fieldOfView, 20.0f, 110.9f);
			}

		}
		else {
			// Get the left mouse button
			if (Input.GetMouseButtonDown(0)) {

				//enabling drag logic  
				touchHander.GetComponent<ObjectDragLogic>().enabled = true;

				// Get mouse origin
				mouseOrigin = Input.mousePosition;
				isRotating = true;
			}

			// Disable movements on button release
			if (!Input.GetMouseButton(0)) isRotating = false;

			// Rotate camera along X and Y axis
			if (isRotating) {
				Vector3 pos = cam.ScreenToViewportPoint(Input.mousePosition - mouseOrigin);

				cam.transform.RotateAround(transformTarget.position, cam.transform.TransformDirection(Vector3.right), -pos.y * turnSpeed);
				cam.transform.RotateAround(transformTarget.position, Vector3.up, pos.x * turnSpeed);
				//               Quaternion q = cam.transform.rotation;
				//               q.eulerAngles = new Vector3(Mathf.Clamp(q.eulerAngles.x, 20.0f, 85.0f), q.eulerAngles.y, 0);

				//               cam.transform.position = new Vector3(cam.transform.position.x, Mathf.Clamp(cam.transform.position.y, 100.0f, 1105.0f), cam.transform.position.z);
				//               cam.transform.rotation = q;

			}

		}

	}
}