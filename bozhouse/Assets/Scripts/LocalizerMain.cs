﻿using System.Collections.Generic;
using System.Globalization;
using System.IO;
using TMPro;
using UnityEngine;

public class LocalizerMain : MonoBehaviour {

    public GameObject settingsMenu;
    public GameObject scoreBoard;
    GameObject[] rootObjects;
    string path;
    
    //Implementing FONT assets;
    TMP_FontAsset latin;
    TMP_FontAsset cyrillic;

    //initializing language text file object
    TextAsset langFile = null;
    TextAsset jsonString = null;

    void Update() {
        
    }

    void Start() {

        //initializing fonts
        latin = Resources.Load("FontAssets/AmaticSC-Regular_ENG", typeof(TMP_FontAsset)) as TMP_FontAsset;
        cyrillic = Resources.Load("FontAssets/AmaticSC_RU", typeof(TMP_FontAsset)) as TMP_FontAsset;

        jsonString = Resources.Load("Lang/LangFlag") as TextAsset;
        LangFlag flag = JsonUtility.FromJson<LangFlag>(jsonString.text);
        
        //OS system lang identifying;
        CultureInfo ci = CultureInfo.InstalledUICulture;

        if (flag.flag == "2" || ci.Name == "ru-RU")  langFile = Resources.Load("Lang/Russian") as TextAsset;
        else if (flag.flag == "1" || ci.Name == "ky-KZ") langFile = Resources.Load("Lang/Kyrgyz") as TextAsset;
        else langFile = Resources.Load("Lang/English") as TextAsset;

        LangJSON languageJson = JsonUtility.FromJson<LangJSON>(langFile.text);
        
        foreach (TextMeshProUGUI txt in FindObjectsOfType<TextMeshProUGUI>()) {
            for(int i = 0; i < languageJson.objectNames.Capacity; i++)
                if (txt.name == languageJson.objectNames[i]) {
                    if (flag.flag == "0") {
                        txt.font = latin;
                        if (txt.name == "PlayButton") txt.font = Resources.Load("FontAssets/AmaticSC-Bold_ENG", typeof(TMP_FontAsset)) as TMP_FontAsset;
                    }
                    else if(flag.flag == "") {
                        txt.font = latin;
                        if (txt.name == "PlayButton") txt.font = Resources.Load("FontAssets/AmaticSC-Bold_ENG", typeof(TMP_FontAsset)) as TMP_FontAsset;
                    }
                    else {
                        txt.font = cyrillic;
                        if (txt.name == "PlayButton") txt.font = Resources.Load("FontAssets/AmaticSC-Bold_KG", typeof(TMP_FontAsset)) as TMP_FontAsset;
                    }
                    txt.text = languageJson.objectTitles[i];
                    break;
                } 
        }

        foreach (TextMeshProUGUI txt in settingsMenu.GetComponentsInChildren<TextMeshProUGUI>()) {
            for (int i = 0; i < languageJson.objectNames.Capacity; i++) {
                if (txt.name == languageJson.objectNames[i]) {
                    if (flag.flag == "0") {
                        txt.font = latin;
                        if (txt.name == "SettingsTitle") txt.font = Resources.Load("FontAssets/AmaticSC-Bold_ENG", typeof(TMP_FontAsset)) as TMP_FontAsset;
                    }
                    else if (flag.flag == "" && ci.Name == "en-US") {
                        txt.font = latin;
                        if (txt.name == "SettingsTitle") txt.font = Resources.Load("FontAssets/AmaticSC-Bold_ENG", typeof(TMP_FontAsset)) as TMP_FontAsset;
                    }
                    else {
                        txt.font = cyrillic;
                        if (txt.name == "SettingsTitle") txt.font = Resources.Load("FontAssets/AmaticSC-Bold_KG", typeof(TMP_FontAsset)) as TMP_FontAsset;
                    }
                    txt.text = languageJson.objectTitles[i];
                    break;
                }
            }  
        }

        foreach (TextMeshProUGUI txt in scoreBoard.GetComponentsInChildren<TextMeshProUGUI>()) {
            for (int i = 0; i < languageJson.objectNames.Capacity; i++) {
                if (txt.name == languageJson.objectNames[i]) {
                    if (flag.flag == "0") {
                        txt.font = latin;
                        if (txt.name == "ScoresTitle") txt.font = Resources.Load("FontAssets/AmaticSC-Bold_ENG", typeof(TMP_FontAsset)) as TMP_FontAsset;
                    }
                    else if (flag.flag == "" && ci.Name == "en-US") {
                        txt.font = latin;
                        if (txt.name == "ScoresTitle") txt.font = Resources.Load("FontAssets/AmaticSC-Bold_ENG", typeof(TMP_FontAsset)) as TMP_FontAsset;
                    }
                    else {
                        txt.font = cyrillic;
                        if (txt.name == "ScoresTitle") txt.font = Resources.Load("FontAssets/AmaticSC-Bold_KG", typeof(TMP_FontAsset)) as TMP_FontAsset;
                    }
                    txt.text = languageJson.objectTitles[i];
                    break;
                }
            }
        }
    }

}

public class LangJSON{

    public List<string> objectNames = new List<string>();
    public List<string> objectTitles = new List<string>();

}


