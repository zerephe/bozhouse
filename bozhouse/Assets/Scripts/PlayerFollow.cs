﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFollow : MonoBehaviour {

    public Transform PlayerTransform;
	public Camera cam;
	public GameObject touchHander;

    private Vector3 _cameraOffset;
    private Vector3 mouseOrigin;
    private Vector3 pos;

    [Range(0.01f, 1.0f)]
    public float SmoothFactor = 0.5f;

    public bool LookAtPlayer = false;

    public bool RotateAroundPlayer = false;

    public float RotationsSpeed = 5.0f;
	public float zoomSpeed = 0.5f;      // Speed of the camera going back and forth


	// Use this for initialization
	void Start () {
		_cameraOffset = cam.transform.position - PlayerTransform.position;	
	}
	
	// LateUpdate is called after Update methods
	void LateUpdate () {

		if (Input.touchCount == 2) {

			//disabling dragging while zooming
			touchHander.GetComponent<ObjectDragLogic> ().enabled = false;

			Touch touchZero = Input.GetTouch (0);
			Touch touchOne = Input.GetTouch (1);

			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

			float prevTouchDeltaMagnitude = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMagnitude = (touchZero.position - touchOne.position).magnitude;
			float deltaMagnitudeDiff = prevTouchDeltaMagnitude - touchDeltaMagnitude;

			if (cam.orthographic) {
				cam.orthographicSize += deltaMagnitudeDiff * zoomSpeed;
				cam.orthographicSize = Mathf.Max (cam.orthographicSize, .1f);
			} else {
				cam.fieldOfView += deltaMagnitudeDiff / 4 * zoomSpeed;
				cam.fieldOfView = Mathf.Clamp (cam.fieldOfView, 20.0f, 100.9f);
			}

		} else { 

            if (Input.GetMouseButtonDown (0)) {

                //enabling dragging while zooming
                touchHander.GetComponent<ObjectDragLogic>().enabled = true;

                RotateAroundPlayer = true;
                mouseOrigin = Input.mousePosition;
                _cameraOffset = cam.transform.position - PlayerTransform.position;
            }
			if (!Input.GetMouseButton(0)) {
				RotateAroundPlayer = false;
			}

				
			if (RotateAroundPlayer) {

                if((int)Input.mousePosition.x != (int)mouseOrigin.x) {
                    Quaternion camTurnAngle =
                        Quaternion.AngleAxis(Input.GetAxis("Mouse X") * RotationsSpeed, Vector3.up);
                    Quaternion camTurnAngle2 =
                        Quaternion.AngleAxis(Input.GetAxis("Mouse Y") * RotationsSpeed, Vector3.left);

                    _cameraOffset = camTurnAngle * (_cameraOffset);
                    Vector3 newPos = PlayerTransform.position + _cameraOffset;

                    cam.transform.position = Vector3.Slerp(cam.transform.position, newPos, SmoothFactor);

                    _cameraOffset = camTurnAngle2 * _cameraOffset;
                    Vector3 newPos2 = PlayerTransform.position + _cameraOffset;
                    cam.transform.position = Vector3.Slerp(cam.transform.position, newPos2, SmoothFactor);

                }

            }


			if (LookAtPlayer || RotateAroundPlayer) {

                cam.transform.LookAt(PlayerTransform);

                if(cam.transform.position.y < 20 || cam.transform.position.y > 90) {
                    cam.transform.position = new Vector3(cam.transform.position.x, Mathf.Clamp(cam.transform.position.y, 20, 90), cam.transform.position.z);
                }
            }
				
		}
	}
}
