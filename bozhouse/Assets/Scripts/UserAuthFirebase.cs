﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using Firebase;
using Firebase.Database;
using Firebase.Unity.Editor;

public class UserAuthFirebase : MonoBehaviour {

    private Firebase.Auth.FirebaseAuth auth;
    private Firebase.Auth.FirebaseUser user;
    private DatabaseReference mReference;

    public GameObject settings;
    
    public TMP_InputField signInEmail;
    public TMP_InputField signInPassword;
    public TMP_InputField signUpEmail;
    public TMP_InputField signUpPassword;
    public TMP_InputField signUpUsername;


    string displayName;
    string emailAddress;

    // Use this for initialization
    void Start () {

        auth = Firebase.Auth.FirebaseAuth.DefaultInstance;

        // Set up the Editor before calling into the realtime database.
        FirebaseApp.DefaultInstance.SetEditorDatabaseUrl("https://bozhouse-001.firebaseio.com/");

        // Get the root reference location of the database.
        mReference = FirebaseDatabase.DefaultInstance.RootReference;

        InitializeFirebase();
        
    }


    public void signIn() {

        string email = signInEmail.GetComponent<TMP_InputField>().text;
        string password = signInPassword.GetComponent<TMP_InputField>().text;

        if (email != "" && password != "") {
            auth.SignInWithEmailAndPasswordAsync(email, password).ContinueWith(task => {
                if (task.IsCanceled) {
                    Debug.LogError("SignInWithEmailAndPasswordAsync was canceled.");
                    return;
                }
                if (task.IsFaulted) {
                    Debug.LogError("SignInWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                    return;
                }

                Firebase.Auth.FirebaseUser newUser = task.Result;
                Debug.LogFormat("User signed in successfully: {0} ({1})",
                    newUser.DisplayName, newUser.UserId);
          
            });
        }
    }

    public void signUp() {


        string email = signUpEmail.GetComponent<TMP_InputField>().text;
        string password = signUpPassword.GetComponent<TMP_InputField>().text;
        string username = signUpUsername.GetComponent<TMP_InputField>().text;
        displayName = username;

        if(email != "" && password != "") {
            auth.CreateUserWithEmailAndPasswordAsync(email, password).ContinueWith(task => {
                if (task.IsCanceled) {
                    Debug.LogError("CreateUserWithEmailAndPasswordAsync was canceled.");
                    return;
                }
                if (task.IsFaulted) {
                    Debug.LogError("CreateUserWithEmailAndPasswordAsync encountered an error: " + task.Exception);
                    return;
                }

                // Firebase user has been created.
                Firebase.Auth.FirebaseUser newUser = task.Result;

                Firebase.Auth.UserProfile profile = new Firebase.Auth.UserProfile {
                    DisplayName = username,
                };

                newUser.UpdateUserProfileAsync(profile).ContinueWith(nameChangeTask => {
                    if (nameChangeTask.IsCanceled) {
                        Debug.LogError("UpdateUserProfileAsync was canceled.");
                        return;
                    }
                    if (nameChangeTask.IsFaulted) {
                        Debug.LogError("UpdateUserProfileAsync encountered an error: " + task.Exception);
                        return;
                    }

                    Debug.LogFormat("Firebase user created successfully: {0} ({1})",
                         newUser.DisplayName, newUser.UserId);

                    //applying new user data into database
                    User usr = new User(email, newUser.DisplayName);
                    string json = JsonUtility.ToJson(usr);
                    mReference.Child("users").Child(newUser.UserId).SetRawJsonValueAsync(json);
                });
            });
        }
    }

    public void signOut() {
        
        auth.SignOut();
        settings.GetComponent<Button>().enabled = true;
        foreach (TextMeshProUGUI txt in settings.GetComponentsInChildren<TextMeshProUGUI>()) {
            if (txt.name.Equals("SignStatus")) txt.text = "Sign In...";
        }
    }

    void InitializeFirebase() {
       
        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }

    void AuthStateChanged(object sender, System.EventArgs eventArgs) {

        if (auth.CurrentUser != user) {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;
            if (!signedIn && user != null) {
                Debug.Log("Signed out " + user.UserId);
                foreach (TextMeshProUGUI txt in settings.GetComponentsInChildren<TextMeshProUGUI>()) {
                    if (txt.name.Equals("SignStatus")) txt.text = "Sign In...";
                }
            }
            user = auth.CurrentUser;
            if (signedIn) {

                if (!user.DisplayName.Trim().Equals("")) {
                    displayName = user.DisplayName;
                }

                emailAddress = user.Email ?? "";
                Debug.Log("Signed in " + user.UserId);
                Debug.Log("Display Name" + displayName);
                Debug.Log("Email Address " + emailAddress);

                
                foreach (TextMeshProUGUI txt in settings.GetComponentsInChildren<TextMeshProUGUI>()) {
                    if (txt.name.Equals("SignStatus")) txt.text = displayName;
                    settings.GetComponent<Button>().enabled = false;
                }
            }
        }
    }

}

public class User {

    public string userEmail;
    public string userName;
    
    public User() { } //default
    
    public User(string userEmail, string userName) {
        this.userEmail = userEmail;
        this.userName = userName;
    }
}