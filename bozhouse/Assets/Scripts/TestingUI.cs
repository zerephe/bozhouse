﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class TestingUI : MonoBehaviour {

    public Button buttonSettings;
    public Button buttonSettingsBack;
    public Button buttonHighScore;
    public Button buttonHighScoreBack;
    public Toggle buttonLangKg;
    public Toggle buttonLangRu;
    public Toggle buttonLangEng;
    public Toggle buttonSound;
    public Toggle buttonMusic;
    public Button buttonExit;

    // Use this for initialization
    void Start () {
        
    }
	
    public void StartTest() {

        StartCoroutine(Test());
       
    }

    IEnumerator Test() {
        yield return new WaitForSeconds(1);
        buttonSettings.onClick.Invoke();
        yield return new WaitForSeconds(1);
        buttonLangRu.onValueChanged.Invoke(true);
        yield return new WaitForSeconds(2);
        buttonSettingsBack.onClick.Invoke();

        yield return new WaitForSeconds(1);
        buttonSettings.onClick.Invoke();
        yield return new WaitForSeconds(1);
        buttonLangEng.onValueChanged.Invoke(true);
        yield return new WaitForSeconds(2);
        buttonSettingsBack.onClick.Invoke();

        yield return new WaitForSeconds(1);
        buttonHighScore.onClick.Invoke();
        yield return new WaitForSeconds(2);
        buttonHighScoreBack.onClick.Invoke();

        yield return new WaitForSeconds(1);
        buttonMusic.onValueChanged.Invoke(true);
        yield return new WaitForSeconds(2);
        buttonMusic.onValueChanged.Invoke(false);


        yield return new WaitForSeconds(1);
        buttonSound.onValueChanged.Invoke(true);
        
        yield return new WaitForSeconds(1);
        buttonSettings.onClick.Invoke();
        yield return new WaitForSeconds(2);
        buttonSettingsBack.onClick.Invoke();

        yield return new WaitForSeconds(2);
        buttonSound.onValueChanged.Invoke(false);

        yield return new WaitForSeconds(1);
        buttonSettings.onClick.Invoke();
        yield return new WaitForSeconds(2);
        buttonSettingsBack.onClick.Invoke();

        yield return new WaitForSeconds(1);
        buttonExit.onClick.Invoke();
    }

    // Update is called once per frame
    void Update () {
		
	}
}
