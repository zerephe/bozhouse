﻿using System.IO;
using TMPro;
using UnityEngine;

public class LocalizerSett : MonoBehaviour {

    public GameObject sett;
    public GameObject scoresBoard;
    GameObject[] rootObjects;
    string path;
    string jsonString, newJson;
    //Implementing FONT assets;
    TMP_FontAsset latin;
    TMP_FontAsset cyrillic;

    public void ChangeLanguage(string lang) {
        
        //initializing fonts
        latin = Resources.Load("FontAssets/AmaticSC-Regular_ENG", typeof(TMP_FontAsset)) as TMP_FontAsset;
        cyrillic = Resources.Load("FontAssets/AmaticSC_RU", typeof(TMP_FontAsset)) as TMP_FontAsset;

        //initializing language text file object
        TextAsset langFile = null;

        LangFlag flag = new LangFlag();
        if (lang == "ru-RU") {
            langFile = Resources.Load("Lang/Russian") as TextAsset;
            flag.flag = "2";
            //File.WriteAllText(Application.dataPath + "/Resources/Lang/LangFlag.json", JsonUtility.ToJson(flag));
        }
        else if (lang == "ky-KZ") {
            langFile = Resources.Load("Lang/Kyrgyz") as TextAsset;
            flag.flag = "1";
            //File.WriteAllText(Application.dataPath + "/Resources/Lang/LangFlag.json", JsonUtility.ToJson(flag));
        }
        else if (lang == "en-US") {
            langFile = Resources.Load("Lang/English") as TextAsset;
            flag.flag = "0";
            //File.WriteAllText(Application.dataPath + "/Resources/Lang/LangFlag.json", JsonUtility.ToJson(flag));
        }
        
        LangJSON languageJson = JsonUtility.FromJson<LangJSON>(langFile.text);
        
        foreach (TextMeshProUGUI txt in sett.GetComponentsInChildren<TextMeshProUGUI>()) {
            for (int i = 0; i < languageJson.objectNames.Capacity; i++) {
                        
                if (txt.name == languageJson.objectNames[i]) {
                    if (flag.flag == "0" || lang == "en-US") {
                        txt.font = latin;
                        if (txt.name == "PlayButton") txt.font = Resources.Load("FontAssets/AmaticSC-Bold_ENG", typeof(TMP_FontAsset)) as TMP_FontAsset;
                    }
                    else {
                        txt.font = cyrillic;
                        if (txt.name == "PlayButton") txt.font = Resources.Load("FontAssets/AmaticSC-Bold_KG", typeof(TMP_FontAsset)) as TMP_FontAsset;
                    }
                    txt.text = languageJson.objectTitles[i];
                    break;
                }
            }
        }
        foreach (TextMeshProUGUI txt in FindObjectsOfType<TextMeshProUGUI>()) {
            for (int i = 0; i < languageJson.objectNames.Capacity; i++) {
                if (txt.name == languageJson.objectNames[i]) {
                    if (flag.flag == "0" || lang == "en-US") {
                        txt.font = latin;
                        if (txt.name == "SettingsTitle") txt.font = Resources.Load("FontAssets/AmaticSC-Bold_ENG", typeof(TMP_FontAsset)) as TMP_FontAsset;
                    }
                    else {
                        txt.font = cyrillic;
                        if (txt.name == "SettingsTitle") txt.font = Resources.Load("FontAssets/AmaticSC-Bold_KG", typeof(TMP_FontAsset)) as TMP_FontAsset;
                    }
                    txt.text = languageJson.objectTitles[i];
                    break;
                }

            }

        }

        foreach (TextMeshProUGUI txt in scoresBoard.GetComponentsInChildren<TextMeshProUGUI>()) {
            for (int i = 0; i < languageJson.objectNames.Capacity; i++) {
                if (txt.name == languageJson.objectNames[i]) {
                    if (flag.flag == "0" || lang == "en-US") {
                        txt.font = latin;
                        if (txt.name == "ScoresTitle") txt.font = Resources.Load("FontAssets/AmaticSC-Bold_ENG", typeof(TMP_FontAsset)) as TMP_FontAsset;
                    }
                    else {
                        txt.font = cyrillic;
                        if (txt.name == "ScoresTitle") txt.font = Resources.Load("FontAssets/AmaticSC-Bold_KG", typeof(TMP_FontAsset)) as TMP_FontAsset;
                    }
                    txt.text = languageJson.objectTitles[i];
                    break;
                }
            }
        }

    }

}

public class LangFlag {
    public string flag;
}
