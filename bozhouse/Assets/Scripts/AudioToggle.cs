﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioToggle : MonoBehaviour {


    public AudioManager audioManager;
    [HideInInspector]
    public bool instance = true, instance2 = true;

    public void ToggleAudio() {
        if (instance) {
            audioManager.gameObject.GetComponents<AudioSource>()[2].mute = true;
            instance = false;
        }
        else {
            audioManager.gameObject.GetComponents<AudioSource>()[2].mute = false;
            instance = true;
        }
    }

    public void ToggleSound() {
        if (instance2) {
            audioManager.gameObject.GetComponents<AudioSource>()[0].mute = true;
            instance2 = false;
        }
        else {
            audioManager.gameObject.GetComponents<AudioSource>()[0].mute = false;
            instance2 = true;
        }
    }

}
