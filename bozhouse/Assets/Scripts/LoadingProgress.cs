﻿using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class LoadingProgress : MonoBehaviour
{
    public TMP_Text textLoad;
    public GameObject loadingScreen;
    public Slider slider;


	public void LoadLevel (int sceneIndex)
	{
	    StartCoroutine(LoadAsynchronously(sceneIndex));
        Debug.Log("block");
	}

    IEnumerator LoadAsynchronously(int sceneIndex)
    {
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneIndex);

        loadingScreen.SetActive(true);

        while (!operation.isDone)
        {
            float progess = Mathf.Clamp01(operation.progress / .9f);

            textLoad.text = ((int)(progess * 100)).ToString() + "%";
        
            slider.value = progess;

            yield return null;
        }
    }
}
