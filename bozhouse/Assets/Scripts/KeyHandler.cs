﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;

public class KeyHandler : MonoBehaviour {
    
	// Update is called once per frame
	void Update () {
        if (Application.platform == RuntimePlatform.Android && Input.GetKey(KeyCode.Escape)) {
            //Quit
            Application.Quit();
        }
    }
}
